# tripleo-standalone-edge

This repository is used for my local testing to deploy TripleO in Standalone
for Edge Computing use-case.

## Challenge

I would like to deploy OpenStack with TripleO but with ephemeral remote
compute nodes, without the need of an undercloud or an overcloud.

My remote compute nodes are on the Edge of the network where the workload
is consumed by my users.

## Architecture

```
+---------------------+         +---------------------+
|standalone|cpu|edge|1|         |standalone|cpu|edge|2|
+---------------------+         +-+-------------------+
                     |             |
                  +--+-------------+-+
                  |standalone|central|
                  +------------------+
```

## How to deploy

I'm using the [official documentation](https://docs.openstack.org/tripleo-docs/latest/install/containers_deployment/standalone.html) to deploy but in this repo you'll find the templates
and roles configuration. They'll eventually move upstream but they stay here
for now as I'm testing this new setup.


### Step 1 - Bootstrap

Deploy the repository on each standalone host:

```bash
$ git clone https://gitlab.com/emacchi/tripleo-standalone-edge
```

Then follow the documentation to deploy tripleo-repos, and install the latest python-tripleoclient.
The whole step 1 has to be done on every standalone node.

### Step 2 - Deploy Central node

Login into the standalone central node and modify the standalone-central.sh
script to match the IP address and local interface of the node.


```bash
$ cp ~/tripleo-standalone-edge/standalone-central.sh ~
$ ./standalone-central.sh
```

Once the standalone-central is deployed, you can go ahead and go to Step 3.

### Step 3 - Deploy a remote Compute node on the Edge

Login into the standalone edge node and modify the standalone-edge.sh
script to match the IP address, local interface of the node, and the hieradata
which is now hardcoded (will be cleaned later) for rpc and memcached.

```bash
$ cp ~/tripleo-standalone-edge/standalone-edge.sh ~
$ sudo cp ~/tripleo-standalone-edge/environments/standalone-edge.yaml /usr/share/openstack-tripleo-heat-templates/environments/standalone.yaml
$ sudo cp ~/tripleo-standalone-edge/roles/Standalone-edge.yaml /usr/share/openstack-tripleo-heat-templates/roles/Standalone.yaml
$ ./standalone-edge.sh
```

Note: for now nova-compute won't start correctly unless you modify by end the
nova.conf in the container to point to the correct keystone endpoint.
I'll work on it next, to clean that up.

## Results

These results are on my setup when I fixed the Keystone configuration in nova.conf.

```
[root@standalone-central ~]# openstack compute service list
+----+------------------+---------------------------------+----------+---------+-------+----------------------------+
| ID | Binary           | Host                            | Zone     | Status  | State | Updated At                 |
+----+------------------+---------------------------------+----------+---------+-------+----------------------------+
|  1 | nova-scheduler   | standalone-central.localdomain  | internal | enabled | up    | 2018-07-19T20:21:01.000000 |
|  2 | nova-consoleauth | standalone-central.localdomain  | internal | enabled | up    | 2018-07-19T20:21:10.000000 |
|  3 | nova-conductor   | standalone-central.localdomain  | internal | enabled | up    | 2018-07-19T20:21:06.000000 |
|  4 | nova-compute     | standalone-central.localdomain  | nova     | enabled | up    | 2018-07-19T20:21:01.000000 |
|  7 | nova-compute     | standalone-cpu-edge.localdomain | nova     | enabled | up    | 2018-07-19T20:21:08.000000 |
+----+------------------+---------------------------------+----------+---------+-------+----------------------------+
[root@standalone-central ~]# openstack network agent list
+--------------------------------------+--------------------+---------------------------------+-------------------+-------+-------+---------------------------+
| ID                                   | Agent Type         | Host                            | Availability Zone | Alive | State | Binary                    |
+--------------------------------------+--------------------+---------------------------------+-------------------+-------+-------+---------------------------+
| 1d530098-f06e-4fe5-b784-38ccc363a3c4 | Metadata agent     | standalone-central.localdomain  | None              | :-)   | UP    | neutron-metadata-agent    |
| 390afcb9-dc05-4388-948f-be2556431e49 | L3 agent           | standalone-central.localdomain  | nova              | :-)   | UP    | neutron-l3-agent          |
| 468d46bd-eb49-4982-898a-201c5a4a24c7 | DHCP agent         | standalone-central.localdomain  | nova              | :-)   | UP    | neutron-dhcp-agent        |
| 54efa09d-ef76-43ab-af9e-e5ade7610445 | Open vSwitch agent | standalone-cpu-edge.localdomain | None              | :-)   | UP    | neutron-openvswitch-agent |
| 97d93fe7-c8bb-4a50-a6e0-fb79960347d3 | Metadata agent     | standalone-cpu-edge.localdomain | None              | :-)   | UP    | neutron-metadata-agent    |
| ede4ea97-4908-42fe-9c54-4352620d196d | Open vSwitch agent | standalone-central.localdomain  | None              | :-)   | UP    | neutron-openvswitch-agent |
+--------------------------------------+--------------------+---------------------------------+-------------------+-------+-------+---------------------------+
```

As you can see, the remote compute node is registred on the central node and now available to schedule servers on it.

## TODO

1. Figure out how to re-use ServiceMap from the central node.
2. Figure out how to properly configure RPC and Memcached.
3. More testing!
